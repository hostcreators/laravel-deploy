@component('mail::message')
# Ahoj světe :)

Dúfam, že sa budeme stretávať častejšie. Toto testujem.

@component('mail::button', ['url' => 'https://www.hostcreators.sk'])
Tlačidlo
@endcomponent

Thx,<br>
HostCreators
@endcomponent
