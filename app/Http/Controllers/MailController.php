<?php

namespace App\Http\Controllers;

use App\Mail\SampleMail;
use App\Mail\SampleMarkdown;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Mail::to('marian.zvalo@hostcreators.sk')->later(now()->addMinutes(1), new SampleMarkdown());
        return redirect('/?r=1');
    }
}
